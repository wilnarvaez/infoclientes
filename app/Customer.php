<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Crypt;

class Customer extends Model
{
    use SoftDeletes;

    protected $table = 'customers';

    protected $fillable = ['name', 'nit', 'direccion', 'telefono', 'city_id', 'cupo', 'saldo_cupo', 'porcentaje_visitas'];

    //desencripta el NIT
    public function getNitAttribute($value)
    {
        return Crypt::decrypt($value);
    }

    //Encripta el NIT
    public function setNitAttribute($value)
    {
        $this->attributes['nit'] = Crypt::encrypt($value);
    }
    
    public function visit(){
        return $this->hasMany(Visit::class);
    }
}
