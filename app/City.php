<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'cities';

    protected $fillable = ['name', 'departament_id'];

    //una ciudad pertenece a un departamento
    public function departament(){
        return $this->belongsTo(Departament::class);
    }
}
