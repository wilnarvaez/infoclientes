<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departament extends Model
{
    protected $table = 'departaments';

    protected $fillable = ['name', 'country_id'];

    protected $dates = ['deleted_at'];

    //un departamento pertenece a un país
    public function county(){
        return $this->belongsTo(Country::class);
    }

    //un departamento tiene N ciudades
    public function cities(){
        return $this->hasMany(City::class);
    }
}
