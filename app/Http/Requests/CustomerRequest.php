<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CustomerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'     => 'required|max:52|regex:/^[\p{L}\s-]+$/',
            'nit'      => 'required|numeric|unique:customers',
            'telefono' => 'numeric',
            'cupo'     => 'numeric',
            'saldo'    => 'numeric',
            'porcentaje_visitas' => 'numeric',
        ];
    }
}
