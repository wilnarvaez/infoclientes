<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VisitRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'seller_id'     => 'required',
            'fecha'         => 'required|date',
            'valor_neto'    => 'required|numeric',
            'customers_id'  => 'required',
        ];
    }
}
