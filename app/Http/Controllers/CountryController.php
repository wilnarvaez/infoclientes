<?php

namespace App\Http\Controllers;

use App\Country;
use App\Http\Requests\CountryRequest;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;

class CountryController extends Controller
{
    public function __construct(Guard $auth)
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $country = Country::all();
            return response()->json($country);
        }
        return view('country.index',['page_title'=>'Paises']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('country.create',['page_title'=>'Paises']);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        if($request->ajax()){
            Country::create([
                'name' => $request['name'],
            ]);

            return response()->json([
                "mensaje" => "Registro Guardado."
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $country = Country::find($id);
        if($country) {
            return response()->json(
                $country
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CountryRequest $request, $id)
    {
        $countries = Country::find($id);
        if($countries){
            $countries->fill($request->all());
            $countries->save();

            return response()->json([
                "mensaje" => "Guardado"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $country = Country::find($id);
        if($country) {
            $country->delete();
            return response()->json([
                "mensaje" => "Eliminado"
            ]);
        }
    }
}
