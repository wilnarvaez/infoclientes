<?php

namespace App\Http\Controllers;

use App\City;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CityController extends Controller
{
    public function __construct(Guard $auth)
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $cities = City::All();
    
            return view('city.index', compact('cities'),['page_title'=>'Ciudades']);
        } catch (\Exception $e) {
            return redirect::to('/ciudades')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            $departaments = \App\Departament::lists('name', 'd');

            return view('city.create', compact('departaments'), ['page_title'=>'Crear Ciudad']);
        } catch (\Exception $e) {
            return redirect::to('/ciudades')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            City::create([
                'name'  => $request['name'],
                'departament_id' => $request['departament_id'],
            ]);

            return redirect('/ciudades')->with('message', 'Nuevo Registro Creado');
        } catch (\Exception $e) {
            return redirect::to('/ciudades')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $departaments = \App\Departament::lists('name', 'id');
            $cities = City::find($id);

            return view('city.edit',compact('cities'),['departaments'=>$departaments,'page_title'=>'Editar Ciudad']);
        } catch (\Exception $e) {
            return redirect::to('/ciudades')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $city = City::find($id);

            $city->fill($request->All());
            $city->save();

            Session::flash('message','Ciudad Actualizada');
            return redirect::to('/ciudades');
        } catch (\Exception $e) {
            return redirect::to('/ciudades')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
