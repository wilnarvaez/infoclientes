<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\CustomerRequest;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    public function __construct(Guard $auth)
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $customers = Customer::All();

            return view('customer.index', compact('customers'),['page_title'=>'Clientes']);
        } catch (\Exception $e) {
            return redirect::to('/dashboard')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            $cities = \App\City::lists('name', 'id');

            return view('customer.create', compact('cities'), ['page_title'=>'Crear Cliente']);
        } catch (\Exception $e) {
            return redirect::to('/clientes')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        try{
            Customer::create([
                'name'      => $request['name'],
                'nit'       => $request['nit'],
                'direccion' => $request['direccion'],
                'telefono'  => $request['telefono'],
                'city_id'   => $request['city_id'],
                'cupo'      => $request['cupo'],
                'saldo_cupo'=> $request['saldo_cupo'],
                'porcentaje_visitas'=> $request['porcentaje_visitas'],
            ]);

            return redirect('/clientes')->with('message', 'Nuevo Registro Creado');
        } catch (\Exception $e) {
            return redirect::to('/clientes')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $cities = \App\City::lists('name', 'id');
            $customers = Customer::find($id);
    
            return view('customer.edit',compact('customers'),['cities'=>$cities,'page_title'=>'Editar Cliente']);
        } catch (\Exception $e) {
            return redirect::to('/clientes')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerRequest $request, $id)
    {
        try{
            $customer = Customer::find($id);
    
            $customer->fill($request->all());
            $customer->save();
    
            Session::flash('message','Registro Actualizado');
            return redirect::to('/clientes');
        } catch (\Exception $e) {
            return redirect::to('/clientes')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            $customer = Customer::find($id);
            $customer->delete();
            Session::flash('message','Registro Eliminado');
            return redirect::to('/clientes');
        } catch (\Exception $e) {
            return redirect::to('/clientes')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        } 
    }
}
