<?php

namespace App\Http\Controllers;

use App\Http\Requests\VisitRequest;
use App\Visit;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class VisitController extends Controller
{
    public function __construct(Guard $auth)
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $visits = Visit::All();

            return view('visit.index', compact('visits'),['page_title'=>'Visitas']);
        } catch (\Exception $e) {
            return redirect::to('/dashboard')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            $sellers = \App\Seller::lists('name', 'id');
            $customers = \App\Customer::lists('name', 'id');

            return view('visit.create', compact('sellers','customers'), ['page_title'=>'Nueva visita']);
        } catch (\Exception $e) {
            return redirect::to('/visitas')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VisitRequest $request)
    {
        try{
            Visit::create([
                'customers_id'  => $request['customers_id'],
                'seller_id'     => $request['seller_id'],
                'valor_neto'    => $request['valor_neto'],
                'fecha'         => $request['fecha'],
                'observaciones' => $request['observaciones'],

            ]);

            return redirect('/visitas')->with('message', 'Nuevo Registro Creado');
        } catch (\Exception $e) {
            return redirect::to('/visitas')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $sellers = \App\Seller::lists('name', 'id');
            $customers = \App\Customer::lists('name', 'id');
            $visits = Visit::find($id);

            return view('visit.edit',compact('visits'),['sellers'=>$sellers,'customers'=>$customers,'page_title'=>'Editar Visita']);
        } catch (\Exception $e) {
            return redirect::to('/visitas')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(VisitRequest $request, $id)
    {
        try{
            $visit = Visit::find($id);

            $visit->fill($request->all());
            $visit->save();

            Session::flash('message','Visita Actualizada');
            return redirect::to('/visitas');
        } catch (\Exception $e) {
            return redirect::to('/visitas')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try{
            //Visit::destroy($id);
            $visit = Visit::find($id);
            $visit->delete();
            Session::flash('message','Registro Eliminado');
            return redirect::to('/visitas');
        } catch (\Exception $e) {
            return redirect::to('/visitas')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }
}
