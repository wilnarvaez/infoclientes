<?php

namespace App\Http\Controllers;

use App\Departament;
use Illuminate\Auth\Guard;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class DepartamentController extends Controller
{
    public function __construct(Guard $auth)
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try{
            $departaments = Departament::All();

            return view('departament.index', compact('departaments'),['page_title'=>'Departamentos']);
        } catch (\Exception $e) {
            return redirect::to('/dashboard')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try{
            $countries = \App\Country::lists('name', 'id');

            return view('departament.create', compact('countries'), ['page_title'=>'Crear Departamento']);
        } catch (\Exception $e) {
            return redirect::to('/departamentos')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            Departament::create([
                'name'  => $request['name'],
                'country_id' => $request['country_id'],
            ]);
    
            return redirect('/departamentos')->with('message', 'Nuevo Registro Creado');
        } catch (\Exception $e) {
            return redirect::to('/departamentos')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{
            $countries = \App\Country::lists('name', 'id');
            $departaments = Departament::find($id);
    
            return view('departament.edit',compact('countries'),['departaments'=>$departaments,'page_title'=>'Editar Departamento']);
        } catch (\Exception $e) {
            return redirect::to('/departamentos')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $departament = Departament::find($id);
    
            $departament->fill($request->all());
            $departament->save();
    
            Session::flash('message','Departamento Actualizado');
            return redirect::to('/departamentos');
        } catch (\Exception $e) {
            return redirect::to('/departamentos')->with('message-error', 'Lo sentimos, algo salió mal. Vuelve a intentarlo, o comunícate con el administrador del sistema.');
        }    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
