<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Seller extends Model
{
    use SoftDeletes;

    protected $table = 'sellers';

    protected $fillable = ['name', 'nit'];

    public function visit(){
        return $this->hasMany(Visit::class);
    }
}
