<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visit extends Model
{
    use SoftDeletes;

    protected $table = 'visits';

    protected $fillable = ['seller_id', 'fecha', 'valor_neto', 'valor_visita', 'observaciones', 'customers_id'];

    protected $dates = ['deleted_at'];

    public function seller(){
        return $this->belongsTo(Seller::class);
    }

    public function customers(){
        return $this->belongsTo(Customer::class);
    }

    /*
     * Permite actualizar el valor de visita cuando se crea
     * o se actualiza el valor neto
     * */
    public function setValorNetoAttribute($value){
        //se toma el id del cliente actual
        $idcliente = intval($this->attributes['customers_id']);

        //busca el porcentaje de visitas del cliente agregado
        $porcentajevisita = Customer::find($idcliente);


        //se calcula el valor de la visita según la formula del cliente
        $valvisita = $value + $porcentajevisita->porcentaje_visitas;

        //se guarda el cálculo
        $this->attributes['valor_visita'] = $valvisita;
        //se pasa el valor sin modificaciones
        $this->attributes['valor_neto'] = $value;
    }


    /*
     * Permite actualizar el valor de visita cuando se crea
     * o se actualiza el customers_id (el cliente)
     * */
//    public function setCustomersIdAttribute($value){
//        //se toma el valor neto
//        dd($this);
//        $valorneto = intval($this->attributes['valor_neto']);
//
//        //busca el porcentaje de visitas del cliente agregado
//        $porcentajevisita = Customer::find($value);
//
//        //se calcula el valor de la visita según la formula del cliente
//        $valvisita = $valorneto + $porcentajevisita->porcentaje_visitas;
//
//        //se guarda el cálculo
//        $this->attributes['valor_visita'] = $valvisita;
//        //se pasa el valor sin modificaciones
//        $this->attributes['customers_id'] = $value;
//    }

}
