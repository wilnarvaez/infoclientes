<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seller_id')->unsigned();
            $table->dateTime('fecha');
            $table->float('valor_neto')->nullable();
            $table->float('valor_visita')->nullable();
            $table->text('observaciones')->nullable();
            $table->integer('customers_id')->unsigned();
            $table->timestamps();

            $table->foreign('seller_id')->references('id')->on('sellers')->onDelete('no action')->onUpdate('no action');
            $table->foreign('customers_id')->references('id')->on('customers')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('visits', function (Blueprint $table) {
            $table->dropForeign(['seller_id']);
            $table->dropForeign(['customers_id']);
        });

        Schema::dropIfExists('visits');
    }
}
