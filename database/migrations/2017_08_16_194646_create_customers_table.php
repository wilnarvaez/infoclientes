<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 52);
            $table->string('nit', 32);
            $table->string('direccion', 45)->nullable();
            $table->string('telefono', 45)->nullable();
            $table->integer('city_id')->unsigned();
            $table->integer('cupo')->nullable();
            $table->float('saldo_cupo')->nullable();
            $table->integer('porcentaje_visitas')->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('city_id')->references('id')->on('cities')->onDelete('no action')->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->dropForeign(['city_id']);
        });

        Schema::dropIfExists('customers');
    }
}
