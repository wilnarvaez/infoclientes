<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

//$factory->define(App\User::class, function (Faker\Generator $faker) {
//    return [
//        'name' => $faker->name,
//        'email' => $faker->safeEmail,
//        'password' => bcrypt(str_random(10)),
//        'remember_token' => str_random(10),
//    ];
//});

$factory->define(App\Seller::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'nit' => $faker->unique()->randomDigit,
    ];
});

$factory->define(App\Customer::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'nit' => $faker->unique()->randomDigit,
        'direccion' => $faker->address,
        'city_id' => $faker->randomElements([1,2,3]),
        'cupo' => $faker->biasedNumberBetween(5,30),
        'salfo_cupo' => $faker->biasedNumberBetween(1000,30000),
        'porcentaje_visitas' => $faker->biasedNumberBetween(1,100),
    ];
});
