$(document).ready(function(){
    Carga();
});
    //agregar paises
    $(function(){
        $("#newcountry").click(function(){
            var tokn = $("#token").val();
            var info = $("#name").val();
            var ruta = "/paises";

            $.ajax({
                url: ruta,
                headers:{'X-CSRF-TOKEN': tokn},
                type: 'POST',
                dataType: 'json',
                data:{name: info},

                success:function(){
                    $("#success").fadeIn();
                },
                error: function (jqXHR, exception) {
                    var msg = '';
                    if (jqXHR.status === 0) {
                        msg = 'No hay conexión.\n Verifique la red.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Página solicitada no encontrada. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Error Interno del Servidor [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Error al analizar JSON solicitado.';
                    } else if (exception === 'timeout') {
                        msg = 'Error de tiempo de espera.';
                    } else if (exception === 'abort') {
                        msg = 'Petición Ajax cancelada.';
                    } else if (jqXHR.responseJSON.name !== '') {
                        msg = jqXHR.responseJSON.name;
                    } else {
                        msg = 'Error no detectado.\n' + jqXHR.responseText;
                    }
                    $("#error").fadeIn();
                    $('#errormsg').html(msg);
                }
            });
        });
    });

    //index paises
    function Carga(){
        var tablaDatos = $("#indexCountry");
        var ruta = "/paises";

        $("#indexCountry").empty();
        $.get(ruta, function(res){
            $(res).each(function(key,value){
                tablaDatos.append("<tr><td>"+value.name+"</td><td><button value="+value.id+" OnClick='Mostrar(this);' class='btn btn-primary' data-toggle='modal' data-target='#myModal'>Editar</button></td><td><button class='btn btn-danger' value="+value.id+" OnClick='Eliminar(this);'>Eliminar</button></td></tr>");
            });
        });
    }

    //mostrar modal de editar país
    function Mostrar(btn){
        var ruta = "/paises/"+btn.value+"/edit";

        $.get(ruta, function(res){
            $("#name").val(res.name);
            $("#id").val(res.id);
        });
    }

    //guarda valor editado de país
    $("#guardar").click(function(){
        var value = $("#id").val();
        var dato = $("#name").val();
        var ruta = "/paises/"+value+"";
        var token = $("#token").val();

        $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': token},
            type: 'PUT',
            dataType: 'json',
            data: {name: dato},
            success: function(){
                Carga();
                $("#myModal").modal('toggle');
                $("#success").fadeIn();
            },
            error: function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'No hay conexión.\n Verifique la red.';
                } else if (jqXHR.status == 404) {
                    msg = 'Página solicitada no encontrada. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Error Interno del Servidor [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Error al analizar JSON solicitado.';
                } else if (exception === 'timeout') {
                    msg = 'Error de tiempo de espera.';
                } else if (exception === 'abort') {
                    msg = 'Petición Ajax cancelada.';
                } else if (jqXHR.responseJSON.name !== '') {
                    msg = jqXHR.responseJSON.name;
                } else {
                    msg = 'Error no detectado.\n' + jqXHR.responseText;
                }
                $("#error").fadeIn();
                $('#errormsg').html(msg);
            }
        });
    });

    //elimina dato de país
    function Eliminar(btn){
        var ruta = "/paises/"+btn.value+"";
        var token = $("#token").val();

        $.ajax({
            url: ruta,
            headers: {'X-CSRF-TOKEN': token},
            type: 'DELETE',
            dataType: 'json',
            success: function(){
                Carga();
                $("#success").fadeIn();
                $('#success').html("Registro Eliminado");
            },
            error: function (jqXHR, exception) {
                var msg = '';
                if (jqXHR.status === 0) {
                    msg = 'No hay conexión.\n Verifique la red.';
                } else if (jqXHR.status == 404) {
                    msg = 'Página solicitada no encontrada. [404]';
                } else if (jqXHR.status == 500) {
                    msg = 'Error Interno del Servidor [500].';
                } else if (exception === 'parsererror') {
                    msg = 'Error al analizar JSON solicitado.';
                } else if (exception === 'timeout') {
                    msg = 'Error de tiempo de espera.';
                } else if (exception === 'abort') {
                    msg = 'Petición Ajax cancelada.';
                } else if (jqXHR.responseJSON.name !== '') {
                    msg = jqXHR.responseJSON.name;
                } else {
                    msg = 'Error no detectado.\n' + jqXHR.responseText;
                }
                $("#error").fadeIn();
                $('#errormsg').html(msg);
            }
        });
    }
