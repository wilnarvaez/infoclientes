@extends('layouts.boots')

@section('content')
@include('country.forms.editmodal')
    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    <div class='form-group'>
        <a href="{{ url('/paises/create')}}" class="btn btn-primary">Agregar País</a>
    </div>
    <br>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nombre</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
                <tbody id="indexCountry"></tbody>
        </table>
    </div>
@stop