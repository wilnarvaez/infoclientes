@extends('layouts.boots')

@section('content')


    {!!Form::model($country,['route'=> ['paises.update', $country->id], 'method'=>'PUT'])!!}

        @include('country.forms.campos')

    {!!Form::close()!!}

@stop