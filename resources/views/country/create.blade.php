@extends('layouts.boots')

@section('content')

    {!!Form::open()!!}
        <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
        @include('country.forms.campos')
        <div class='form-group'>
            {!!link_to('#',$title='Guardar', $attributes = ['id'=>'newcountry', 'class'=>'btn btn-primary'], $secure = null)!!}
        </div>

    {!!Form::close()!!}

@stop