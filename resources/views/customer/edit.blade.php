@extends('layouts.boots')

@section('content')


    {!!Form::model($customers,['route'=> ['clientes.update', $customers->id], 'method'=>'PUT'])!!}

        @include('customer.forms.campos')

    {!!Form::close()!!}

@stop