<div class='form-group'>
    {!!Form::label('Nombre Completo')!!}
    {!!Form::text('name',null,['class'=>'form-control'])!!}
</div>

<div class='form-group'>
    {!!Form::label('NIT')!!}
    {!!Form::text('nit',null,['class'=>'form-control','placeholder'=>'Identificación del cliente'])!!}
</div>

<div class='form-group'>
    {!!Form::label('Dirección')!!}
    {!!Form::text('direccion',null,['class'=>'form-control'])!!}
</div>

<div class='form-group'>
    {!!Form::label('Teléfono')!!}
    {!!Form::text('telefono',null,['class'=>'form-control'])!!}
</div>

<div class='form-group'>
    {!!Form::label('Ciudad')!!}
    {!!Form::select('city_id', $cities, null, array('class' => 'form-control'))!!}
</div>

<div class='form-group'>
    {!!Form::label('Cupo')!!}
    {!!Form::text('cupo',null,['class'=>'form-control'])!!}
</div>

<div class='form-group'>
    {!!Form::label('Saldo Cupo')!!}
    {!!Form::text('saldo_cupo',null,['class'=>'form-control'])!!}
</div>

<div class='form-group'>
    {!!Form::label('Porcentaje de Visitas')!!}
    {!!Form::text('porcentaje_visitas',null,['class'=>'form-control'])!!}
</div>

<div class='form-group'>
    {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!}
</div>