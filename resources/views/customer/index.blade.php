@extends('layouts.boots')

@section('content')
    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    <div class='form-group'>
        <a href="{{ url('/clientes/create')}}" class="btn btn-primary">Agregar Cliente</a>
    </div>
    <br>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>NIT</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Ciudad</th>
                <th>Cupo</th>
                <th>Saldo Cupo</th>
                <th>Porcentaje Visitas</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            @foreach($customers as $customer)
                <tbody>
                <tr>
                    <td>{{$customer->name}}</td>
                    <td>{{$customer->nit}}</td>
                    <td>{{$customer->direccion}}</td>
                    <td>{{$customer->telefono}}</td>
                    <td>{{$customer->city_id}}</td>
                    <td>{{$customer->cupo}}</td>
                    <td>{{$customer->saldo_cupo}}</td>
                    <td>{{$customer->porcentaje_visitas}}%</td>
                    <td>{!!link_to_route('clientes.edit', $title = 'Editar', $parameters = $customer->id, $atributes = ['class'=>'btn btn-primary'])!!}</td>
                    <td>
                        {!!Form::open(['route'=>['clientes.destroy', $customer->id],'method'=>'DELETE'])!!}
                            <div class='form-group'>
                                {!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
                            </div>
                        {!!Form::close()!!}
                    </td>
                </tr>
                </tbody>
            @endforeach
        </table>
    </div>

@stop