@extends('layouts.boots')

@section('content')

    {!!Form::model($visits,['route'=> ['visitas.update', $visits->id], 'method'=>'PUT'])!!}

        @include('visit.forms.campos')

    {!!Form::close()!!}

    {!!Form::open(['route'=>['visitas.destroy', $visits->id],'method'=>'DELETE'])!!}
        {!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
    {!!Form::close()!!}

@stop