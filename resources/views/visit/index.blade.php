@extends('layouts.boots')

@section('content')
    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    <div class='form-group'>
        <a href="{{ url('/visitas/create')}}" class="btn btn-primary">Agregar Visita</a>
    </div>
    <br>

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Cliente</th>
                <th>Vendedor</th>
                <th>Fecha de Venta</th>
                <th>Valor Neto</th>
                <th>Valor Visita</th>
                <th>Observaciones</th>
                <th></th>
            </tr>
            </thead>
            @foreach($visits as $visit)
                <tbody>
                <tr>
                    <td>{{$visit->customers->name}}</td>
                    <td>{{$visit->seller->name}}</td>
                    <td>{{$visit->fecha}}</td>
                    <td>{{$visit->valor_neto}}</td>
                    <td>{{$visit->valor_visita}}</td>
                    <td>{{$visit->observaciones}}</td>
                    <td>{!!link_to_route('visitas.edit', $title = 'Editar', $parameters = $visit->id, $atributes = ['class'=>'btn btn-primary'])!!}</td>
                    <td>
                        {!!Form::open(['route'=>['visitas.destroy', $visit->id],'method'=>'DELETE'])!!}
                            <div class='form-group'>
                                {!!Form::submit('Eliminar',['class'=>'btn btn-danger'])!!}
                            </div>
                        {!!Form::close()!!}
                    </td>
                </tr>
                </tbody>
            @endforeach
        </table>
    </div>

@stop