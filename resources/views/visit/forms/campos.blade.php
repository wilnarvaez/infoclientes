<div class='form-group'>
    {!!Form::label('Vendedor')!!}
    {!!Form::select('seller_id', $sellers, null, array('class' => 'form-control'))!!}
</div>

<div class='form-group'>
    {!!Form::label('Fecha')!!}
    {!!Form::text('fecha',null,['class'=>'form-control'])!!}
</div>

<div class='form-group'>
    {!!Form::label('Valor Neto')!!}
    {!!Form::text('valor_neto',null,['class'=>'form-control'])!!}
</div>

<div class='form-group'>
    {!!Form::label('Observaciones')!!}
    {!!Form::text('observaciones',null,['class'=>'form-control'])!!}
</div>

<div class='form-group'>
    {!!Form::label('Cliente')!!}
    {!!Form::select('customers_id', $customers, null, array('class' => 'form-control'))!!}
</div>

<div class='form-group'>
    {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!}
</div>