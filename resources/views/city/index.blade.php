@extends('layouts.boots')

@section('content')
    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nombre</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            @foreach($cities as $city)
                <tbody>
                <tr>
                    <td>{{$city->name}}</td>
                    <td>{!!link_to_route('ciudades.edit', $title = 'Editar', $parameters = $city->id, $atributes = ['class'=>'btn btn-primary'])!!}</td>
                </tr>
                </tbody>
            @endforeach
        </table>
    </div>

@stop