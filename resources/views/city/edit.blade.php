@extends('layouts.boots')

@section('content')


    {!!Form::model($cities,['route'=> ['ciudades.update', $cities->id], 'method'=>'PUT'])!!}

        @include('city.forms.campos')

    {!!Form::close()!!}

@stop