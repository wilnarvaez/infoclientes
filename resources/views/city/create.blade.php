@extends('layouts.boots')

@section('content')

    {!!Form::open(['route'=>'ciudades.store','method'=>'POST'])!!}

        @include('city.forms.campos')

    {!!Form::close()!!}

@stop