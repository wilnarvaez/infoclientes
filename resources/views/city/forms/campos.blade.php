<div class='form-group'>
    {!!Form::label('Departamento')!!}
    {!!Form::select('departament_id', $departaments, null, array('class' => 'form-control'))!!}
</div>

<div class='form-group'>
    {!!Form::label('Ciudad')!!}
    {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingrese una ciudad'])!!}
</div>

<div class='form-group'>
    {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!}
</div>