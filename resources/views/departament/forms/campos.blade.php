<div class='form-group'>
    {!!Form::label('País')!!}
    {!!Form::select('country_id', $countries, null, array('class' => 'form-control'))!!}
</div>

<div class='form-group'>
    {!!Form::label('Departamento')!!}
    {!!Form::text('name',null,['class'=>'form-control','placeholder'=>'Ingrese un departamento'])!!}
</div>

<div class='form-group'>
    {!!Form::submit('Guardar',['class'=>'btn btn-primary'])!!}
</div>