@extends('layouts.boots')

@section('content')
    @if(Session::has('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{Session::get('message')}}
        </div>
    @endif

    <div class="table-responsive">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Nombre</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            @foreach($departaments as $departament)
                <tbody>
                <tr>
                    <td>{{$departament->name}}</td>
                    <td>{!!link_to_route('departamentos.edit', $title = 'Editar', $parameters = $departament->id, $atributes = ['class'=>'btn btn-primary'])!!}</td>
                </tr>
                </tbody>
            @endforeach
        </table>
    </div>

@stop