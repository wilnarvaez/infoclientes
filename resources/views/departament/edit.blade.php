@extends('layouts.boots')

@section('content')


    {!!Form::model($departaments,['route'=> ['departamentos.update', $departaments->id], 'method'=>'PUT'])!!}

        @include('departament.forms.campos')

    {!!Form::close()!!}

@stop